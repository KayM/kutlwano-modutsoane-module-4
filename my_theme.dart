import 'package:flutter/material.dart';

class MyTheme {
  static ThemeData get lightTheme {
    return ThemeData(
      scaffoldBackgroundColor: Colors.white,
      fontFamily: 'Montserrat',
      buttonTheme: ButtonThemeData(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
      ),
      colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.lightGreen)
          .copyWith(secondary: Colors.green[300]),
    );
  }
}
