// ignore: unused_import
import 'package:flutter/material.dart';
import 'package:module3_app/dashboard.dart';
import 'package:module3_app/pure_fumes.dart';
import 'package:module3_app/my_theme.dart';

class DiffuserBlends extends StatelessWidget {
  const DiffuserBlends({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Diffuser Blends',
      theme: MyTheme.lightTheme,
      home: Scaffold(
        appBar: AppBar(
          title: const Center(
            child: Text('Aromatic Diffuser Blends'),
          ),
          leading: IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context,
                    MaterialPageRoute(builder: (context) => const Dashboard()));
              }),
        ),
        body: const Center(
          child: Text('Diffuser Blends'),
        ),
        floatingActionButton: FloatingActionButton(
          tooltip: 'Next Page',
          child: const Icon(Icons.navigate_next),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const PureFumes()));
          },
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
