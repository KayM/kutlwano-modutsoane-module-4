import 'package:flutter/material.dart';
import 'package:module3_app/diffuser_blends.dart';
import 'package:module3_app/profile_edit.dart';
import 'package:module3_app/my_theme.dart';

class PureFumes extends StatelessWidget {
  const PureFumes({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Aromatic PureFumes',
      theme: MyTheme.lightTheme,
      home: Scaffold(
        appBar: AppBar(
          title: const Center(child: Text('PureFumes')),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const DiffuserBlends()));
            },
          ),
        ),
        body: const Center(
          child: Text('Perfume Blends'),
        ),
        floatingActionButton: FloatingActionButton(
          tooltip: 'Next Page',
          child: const Icon(Icons.navigate_next),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const ProfileEditRoute()));
          },
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
