import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:module3_app/log_in_route.dart';
import 'package:page_transition/page_transition.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  static const String title = 'Login Page';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Chakra Charm',
        home: AnimatedSplashScreen(
          splash: Column(
            children: [
              Center(
                child:
                    Image.asset('assets/images/essential_oils_icon_3-01.jpg'),
              ),
            ],
          ),
          splashIconSize: 400,
          duration: 2000,
          splashTransition: SplashTransition.scaleTransition,
          pageTransitionType: PageTransitionType.leftToRightWithFade,
          nextScreen: const LogInRoute(),
          animationDuration: const Duration(
            seconds: 1,
          ),
        ),
        debugShowCheckedModeBanner: false);
  }
}
