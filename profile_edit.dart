import 'package:flutter/material.dart';
import 'package:module3_app/my_theme.dart';
import 'package:module3_app/pure_fumes.dart';

class ProfileEditRoute extends StatelessWidget {
  const ProfileEditRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chakra Charm',
      theme: MyTheme.lightTheme,
      home: Scaffold(
        appBar: AppBar(
          title: const Center(child: Text('User Profile')),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context,
                  MaterialPageRoute(builder: (context) => const PureFumes()));
            },
          ),
        ),
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircleAvatar(
                    minRadius: 60.0,
                    backgroundColor: Colors.green[300],
                    child: const CircleAvatar(
                      radius: 50.0,
                      backgroundImage: AssetImage('assets/images/boo.jpg'),
                    ),
                  ),
                ],
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Name',
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Email',
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: 'Password',
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  maxLength: null,
                  maxLines: null,
                  decoration: InputDecoration(
                    hintText: 'Bio',
                  ),
                ),
              ),
            ]),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
